# Money transfer service
Basic money transfer service with minimal amount of dependencies.

## How to use

### Build
```sh
mvn clean package
```

### Run
```sh
java -jar target/money-transfer-1.0-jar-with-dependencies.jar
```

## Short API overview
- PUT /register
: register new account in the application; returns account
- POST /deposit
: add funds to an account; returns account
- POST /transfer
: transfer funds between accounts; returns transaction
- GET /get-account
: return info about account
- GET /get-transaction
: return info about transaction

## Usage example
```sh
$ curl -X PUT localhost:8000/register
{
  "id": 1,
  "balance": 0.0
}
$ curl -X PUT localhost:8000/register
{
  "id": 2,
  "balance": 0.0
}
$ curl -X POST "localhost:8000/deposit?id=1&volume=10"
{
  "id": 1,
  "balance": 10.0
}
$ curl -X POST "localhost:8000/deposit?id=2&volume=20"
{
  "id": 2,
  "balance": 20.0
}
$ curl -X POST "localhost:8000/transfer?from=2&to=1&volume=15"
{
  "id": 1,
  "fromId": 2,
  "toId": 1,
  "volume": 15.0
}
$ curl localhost:8000/get-account?id=1
{
  "id": 1,
  "balance": 25.0
}
$ curl localhost:8000/get-account?id=2
{
  "id": 2,
  "balance": 5.0
}
$ curl localhost:8000/get-transaction?id=1
{
  "id": 1,
  "fromId": 2,
  "toId": 1,
  "volume": 15.0
}
```
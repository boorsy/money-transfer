package org.bursy.moneytransfer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpServer;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.bursy.moneytransfer.model.Account;
import org.bursy.moneytransfer.model.Transaction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MoneyTransferTestSuite {
    private static int TEST_PORT = 7999;
    private static int TEST_THREADS = 5;

    private static String BASE_URL = "http://localhost:" + TEST_PORT + "/";

    private HttpServer server;

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private Random random = new Random();

    @BeforeEach
    void setUp() throws Exception {
        server = HttpServerConfigurer.createHttpServer(TEST_PORT, TEST_THREADS);
        server.start();
    }

    @AfterEach
    void tearDown() {
        server.stop(0);
    }

    @Test
    void testRegister() throws Exception {
        executeRegister(createSingleClient());
    }

    @Test
    void testDeposit() throws Exception {
        int depositCount = 100;

        HttpClient client = createSingleClient();
        Account account = executeRegister(client);

        double expectedTotal = 0d;
        for (int i = 0; i < depositCount; i++) {
            double volume = random.nextDouble();
            executeDeposit(client, account.getId(), volume);
            expectedTotal += volume;
        }

        Account finalAccount = executeGetAccount(client, account.getId());

        assertThat("Account volume is off", finalAccount.getBalance(), equalTo(expectedTotal));
    }

    @Test
    void testTransfer() throws Exception {
        int accountCount = 100;
        int totalTransactionCount = 1000;
        int clientsCount = 50;

        double[] expectedValues = new double[accountCount];
        HttpClient[] clients = IntStream.range(0, clientsCount)
                .mapToObj(i -> createSingleClient())
                .toArray(HttpClient[]::new);

        for (int i = 1; i <= accountCount; i++) {
            executeRegister(clients[0]);
        }

        int transactionCount = 0;
        while (transactionCount < totalTransactionCount) {
            int from = random.nextInt(accountCount - 1) + 1;
            int to = random.nextInt(accountCount - 1) + 1;

            if (from == to) {
                continue;
            }

            double volume = random.nextDouble();
            expectedValues[from - 1] -= volume;
            expectedValues[to - 1] += volume;

            executeTransfer(clients[transactionCount % clientsCount], from, to, volume);
            transactionCount++;
        }

        for (int i = 1; i <= accountCount; i++) {
            Account account = executeGetAccount(clients[0], i);

            assertThat("Account volume is off", account.getBalance(), equalTo(expectedValues[i - 1]));
        }
    }

    private Account executeRegister(HttpClient client) throws Exception {
        URI uri = createURI("register");
        HttpPut request = new HttpPut(uri);
        return getAccountResult(client, request);
    }

    private Account executeDeposit(HttpClient client, Integer id, Double volume) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("id", id.toString());
        params.put("volume", volume.toString());

        URI uri = createURI("deposit", params);
        HttpPost request = new HttpPost(uri);
        return getAccountResult(client, request);
    }

    private Account executeGetAccount(HttpClient client, Integer id) throws Exception {
        URI uri = createURI("get-account", Collections.singletonMap("id", id.toString()));
        HttpGet request = new HttpGet(uri);
        return getAccountResult(client, request);
    }

    private Transaction executeTransfer(HttpClient client, Integer from, Integer to, Double volume) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("from", from.toString());
        params.put("to", to.toString());
        params.put("volume", volume.toString());

        URI uri = createURI("transfer", params);
        HttpPost request = new HttpPost(uri);
        return getTransactionResult(client, request);
    }

    private URI createURI(String path) throws Exception {
        return new URIBuilder(BASE_URL + path).build();
    }

    private URI createURI(String path, Map<String, String> params) throws Exception {
        URIBuilder builder = new URIBuilder(BASE_URL + path);
        params.forEach(builder::setParameter);
        return builder.build();
    }

    private Account getAccountResult(HttpClient client, HttpUriRequest request) throws Exception {
        return gson.fromJson(EntityUtils.toString(client.execute(request).getEntity()), Account.class);
    }

    private Transaction getTransactionResult(HttpClient client, HttpUriRequest request) throws Exception {
        return gson.fromJson(EntityUtils.toString(client.execute(request).getEntity()), Transaction.class);
    }

    private HttpClient createSingleClient() {
        return HttpClientBuilder.create().build();
    }
}

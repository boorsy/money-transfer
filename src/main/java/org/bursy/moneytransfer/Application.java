package org.bursy.moneytransfer;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

public class Application {
    private static String PROPERTIES_FILE = "conf.properties";

    private static int DEFAULT_APP_PORT = 8000;
    private static int DEFAULT_THREADS = 1;

    public static void main(String[] args) throws IOException {
        Properties properties = loadProperties();

        int port = getIntProperty(properties, "application.port", DEFAULT_APP_PORT);
        int threads = getIntProperty(properties, "application.threads", DEFAULT_THREADS);
        HttpServer server = HttpServerConfigurer.createHttpServer(port, threads);

        System.out.println("Starting app on port " + server.getAddress().getPort());

        server.start();
    }

    private static Properties loadProperties() {
        Properties properties = new Properties();

        try (InputStream inputStream = Application.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
            properties.load(inputStream);
        } catch (Exception e) {
            System.out.println("Unable to load properties, falling back to default settings");
        }

        return properties;
    }

    private static int getIntProperty(Properties properties, String propertyName, int defaultValue) {
        return Optional.ofNullable(properties.getProperty(propertyName))
                .map(Integer::valueOf)
                .orElse(defaultValue);
    }
}

package org.bursy.moneytransfer.service;

import org.bursy.moneytransfer.dao.TransactionDao;
import org.bursy.moneytransfer.exception.NotFoundException;
import org.bursy.moneytransfer.model.Transaction;

import java.util.concurrent.atomic.AtomicInteger;

public class TransactionService {
    private final TransactionDao transactionDao;
    private final AtomicInteger currentId;

    public TransactionService(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
        this.currentId = new AtomicInteger();
    }

    public Transaction createTransaction(Integer fromId, Integer toId, double volume) {
        Transaction transaction = new Transaction(currentId.incrementAndGet(), fromId, toId, volume);
        transactionDao.addTransaction(transaction);
        return transaction;
    }

    public Transaction getAndCheckTransaction(Integer id) {
        Transaction transaction = transactionDao.getTransaction(id);

        if (transaction == null) {
            throw new NotFoundException("Transaction with id " + id + " doesn't exist");
        }

        return transaction;
    }
}

package org.bursy.moneytransfer.service;

import org.bursy.moneytransfer.dao.AccountDao;
import org.bursy.moneytransfer.exception.NotFoundException;
import org.bursy.moneytransfer.exception.BadRequestException;
import org.bursy.moneytransfer.model.Account;
import org.bursy.moneytransfer.model.Transaction;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class AccountService {
    private final AccountDao accountDao;
    private final TransactionService transactionService;
    private final AtomicInteger currentId;

    public AccountService(AccountDao accountDao, TransactionService transactionService) {
        this.accountDao = accountDao;
        this.transactionService = transactionService;
        this.currentId = new AtomicInteger();
    }

    public Account register() {
        // id clashes are prevented by using a single atomic for id generation
        Account account = new Account(currentId.incrementAndGet());
        accountDao.addAccount(account);
        return account;
    }

    public Account deposit(Integer id, double volume) {
        Account account = getAndCheckAccount(id);

        // we can use the account object as lock due to the fact storage is in-memory
        synchronized (account) {
            accountDao.changeBalance(account, volume);

            return account;
        }
    }

    public Account getAndCheckAccount(Integer id) {
        Account account = accountDao.getAccount(id);

        if (account == null) {
            throw new NotFoundException("Account with id " + id + " doesn't exist");
        }

        return account;
    }

    public Transaction transfer(Integer fromId, Integer toId, double volume) {
        if (fromId.equals(toId)) {
            throw new BadRequestException("Can't transfer funds to yourself");
        }

        Account from = getAndCheckAccount(fromId);
        Account to = getAndCheckAccount(toId);

        return withDoubleLock(from, to, () -> {
            accountDao.changeBalance(from, -volume);
            accountDao.changeBalance(to, volume);

            return transactionService.createTransaction(fromId, toId, volume);
        });
    }

    private <T> T withDoubleLock(Account first, Account second, Supplier<T> callback) {
        Account lowerIdAccount = first.getId() < second.getId() ? first : second;
        Account higherIdAccount = first.getId() < second.getId() ? second : first;

        // with locks ordered by ids we can easily prevent any deadlocks
        synchronized (lowerIdAccount) {
            synchronized (higherIdAccount) {
                return callback.get();
            }
        }
    }
}

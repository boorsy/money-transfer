package org.bursy.moneytransfer.exception;

import java.net.HttpURLConnection;

public class BadRequestException extends HttpCodeException {
    public BadRequestException(String message) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, message);
    }
}

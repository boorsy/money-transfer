package org.bursy.moneytransfer.exception;

import java.net.HttpURLConnection;

public class NotFoundException extends HttpCodeException {
    public NotFoundException(String message) {
        super(HttpURLConnection.HTTP_NOT_FOUND, message);
    }
}

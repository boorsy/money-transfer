package org.bursy.moneytransfer.exception;

public class HttpCodeException extends RuntimeException {
    private int httpCode;

    public HttpCodeException(int code) {
        super();
        this.httpCode = code;
    }

    public HttpCodeException(int code, String message) {
        super(message);
        this.httpCode = code;
    }

    public HttpCodeException(int code, String message, Throwable t) {
        super(message, t);
        this.httpCode = code;
    }

    public int getHttpCode() {
        return httpCode;
    }
}

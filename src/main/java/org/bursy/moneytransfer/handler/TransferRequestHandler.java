package org.bursy.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;
import org.bursy.moneytransfer.exception.BadRequestException;
import org.bursy.moneytransfer.model.Transaction;
import org.bursy.moneytransfer.service.AccountService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

public class TransferRequestHandler extends RequestHandler {
    private final AccountService accountService;

    public TransferRequestHandler(AccountService accountService) {
        this.accountService = accountService;
    }

    public String getMethod() {
        return "POST";
    }

    protected void handleInternally(HttpExchange exchange) throws IOException {
        Integer fromId;
        Integer toId;
        double volume;

        try {
            Map<String, String> params = getParams(exchange);
            fromId = Integer.valueOf(params.get("from"));
            toId = Integer.valueOf(params.get("to"));
            volume = Double.valueOf(params.get("volume"));
        } catch (Exception e) {
            throw new BadRequestException("Params are invalid");
        }

        Transaction transaction = accountService.transfer(fromId, toId, volume);

        writeJsonResponse(exchange, HttpURLConnection.HTTP_OK, transaction);
    }
}

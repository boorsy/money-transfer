package org.bursy.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;
import org.bursy.moneytransfer.exception.BadRequestException;
import org.bursy.moneytransfer.model.Account;
import org.bursy.moneytransfer.service.AccountService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

public class GetAccountRequestHandler extends RequestHandler {
    private final AccountService accountService;

    public GetAccountRequestHandler(AccountService accountService) {
        this.accountService = accountService;
    }

    public String getMethod() {
        return "GET";
    }

    protected void handleInternally(HttpExchange exchange) throws IOException {
        Integer id;

        try {
            Map<String, String> params = getParams(exchange);
            id = Integer.valueOf(params.get("id"));
        } catch (Exception e) {
            throw new BadRequestException("Params are invalid");
        }

        Account account = accountService.getAndCheckAccount(id);

        writeJsonResponse(exchange, HttpURLConnection.HTTP_OK, account);
    }
}

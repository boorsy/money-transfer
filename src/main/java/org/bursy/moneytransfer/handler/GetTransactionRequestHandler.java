package org.bursy.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;
import org.bursy.moneytransfer.exception.BadRequestException;
import org.bursy.moneytransfer.model.Transaction;
import org.bursy.moneytransfer.service.TransactionService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

public class GetTransactionRequestHandler extends RequestHandler {
    private final TransactionService transactionService;

    public GetTransactionRequestHandler(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    public String getMethod() {
        return "GET";
    }

    protected void handleInternally(HttpExchange exchange) throws IOException {
        Integer id;

        try {
            Map<String, String> params = getParams(exchange);
            id = Integer.valueOf(params.get("id"));
        } catch (Exception e) {
            throw new BadRequestException("Params are invalid");
        }

        Transaction transaction = transactionService.getAndCheckTransaction(id);

        writeJsonResponse(exchange, HttpURLConnection.HTTP_OK, transaction);
    }
}

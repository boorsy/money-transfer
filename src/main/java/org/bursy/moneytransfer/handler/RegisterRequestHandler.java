package org.bursy.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;
import org.bursy.moneytransfer.model.Account;
import org.bursy.moneytransfer.service.AccountService;

import java.io.IOException;
import java.net.HttpURLConnection;

public class RegisterRequestHandler extends RequestHandler {
    private final AccountService accountService;

    public RegisterRequestHandler(AccountService accountService) {
        this.accountService = accountService;
    }

    public String getMethod() {
        return "PUT";
    }

    protected void handleInternally(HttpExchange exchange) throws IOException {
        Account account = accountService.register();

        writeJsonResponse(exchange, HttpURLConnection.HTTP_CREATED, account);
    }
}

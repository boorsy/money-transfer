package org.bursy.moneytransfer.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.bursy.moneytransfer.exception.BadRequestException;
import org.bursy.moneytransfer.exception.HttpCodeException;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class RequestHandler implements HttpHandler {
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public abstract String getMethod();
    protected abstract void handleInternally(HttpExchange exchange) throws IOException;

    public void handle(HttpExchange exchange) throws IOException {
        try {
            checkMethod(exchange, getMethod());

            handleInternally(exchange);
        } catch (HttpCodeException e) {
            writeResponse(exchange, e.getHttpCode(), e.getMessage());
        } catch (Exception e) {
            writeResponse(exchange, HttpURLConnection.HTTP_INTERNAL_ERROR, e.getMessage());
        }
    }

    protected void writeResponse(HttpExchange exchange, int httpCode, String body) throws IOException {
        exchange.sendResponseHeaders(httpCode, body.length());
        OutputStream os = exchange.getResponseBody();
        os.write(body.getBytes());
        os.close();
    }

    protected void writeJsonResponse(HttpExchange exchange, int httpCode, Object object) throws IOException {
        writeResponse(exchange, httpCode, gson.toJson(object));
    }

    protected void checkMethod(HttpExchange exchange, String allowed) throws HttpCodeException {
        if (!allowed.equals(exchange.getRequestMethod())) {
            throw new HttpCodeException(HttpURLConnection.HTTP_BAD_METHOD,
                    exchange.getRequestMethod() + " method not allowed");
        }
    }

    protected Map<String, String> getParams(HttpExchange exchange) {
        try {
            return Arrays.stream(exchange.getRequestURI().getQuery()
                    .split("&"))
                    .map(s -> s.split("="))
                    .collect(Collectors.toMap(chunks -> chunks[0], chunks -> chunks[1]));
        } catch (Exception e) {
            throw new BadRequestException("Query string is invalid");
        }
    }
}

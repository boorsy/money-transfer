package org.bursy.moneytransfer.model;

public class Account {
    private final Integer id;
    private double balance;

    public Account(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}

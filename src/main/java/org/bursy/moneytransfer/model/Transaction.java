package org.bursy.moneytransfer.model;

public class Transaction {
    private final Integer id;
    private final Integer fromId;
    private final Integer toId;
    private final double volume;

    public Transaction(Integer id, Integer fromId, Integer toId, double volume) {
        this.id = id;
        this.fromId = fromId;
        this.toId = toId;
        this.volume = volume;
    }

    public Integer getId() {
        return id;
    }

    public Integer getFromId() {
        return fromId;
    }

    public Integer getToId() {
        return toId;
    }

    public double getVolume() {
        return volume;
    }
}

package org.bursy.moneytransfer.dao;

import org.bursy.moneytransfer.model.Account;

import java.util.concurrent.ConcurrentHashMap;

public class AccountDao {
    private final ConcurrentHashMap<Integer, Account> accounts;

    public AccountDao() {
        this.accounts = new ConcurrentHashMap<>();
    }

    public void addAccount(Account account) {
        accounts.put(account.getId(), account);
    }

    public Account getAccount(Integer id) {
        return accounts.get(id);
    }

    public void changeBalance(Account account, double volume) {
        // that's located in dao only for the consistency with dao principles
        account.setBalance(account.getBalance() + volume);
    }
}

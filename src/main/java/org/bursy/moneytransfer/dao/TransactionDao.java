package org.bursy.moneytransfer.dao;

import org.bursy.moneytransfer.model.Transaction;

import java.util.concurrent.ConcurrentHashMap;

public class TransactionDao {
    private final ConcurrentHashMap<Integer, Transaction> transactions;

    public TransactionDao() {
        this.transactions = new ConcurrentHashMap<>();
    }

    public void addTransaction(Transaction transaction) {
        transactions.put(transaction.getId(), transaction);
    }

    public Transaction getTransaction(Integer id) {
        return transactions.get(id);
    }
}

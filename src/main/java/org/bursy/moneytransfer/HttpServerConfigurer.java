package org.bursy.moneytransfer;

import com.sun.net.httpserver.HttpServer;
import org.bursy.moneytransfer.dao.AccountDao;
import org.bursy.moneytransfer.dao.TransactionDao;
import org.bursy.moneytransfer.handler.*;
import org.bursy.moneytransfer.service.AccountService;
import org.bursy.moneytransfer.service.TransactionService;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class HttpServerConfigurer {
    public static HttpServer createHttpServer(int port, int threads) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);

        AccountDao accountDao = new AccountDao();
        TransactionDao transactionDao = new TransactionDao();
        TransactionService transactionService = new TransactionService(transactionDao);
        AccountService accountService = new AccountService(accountDao, transactionService);

        server.createContext("/register", new RegisterRequestHandler(accountService));
        server.createContext("/deposit", new DepositRequestHandler(accountService));
        server.createContext("/get-account", new GetAccountRequestHandler(accountService));
        server.createContext("/transfer", new TransferRequestHandler(accountService));
        server.createContext("/get-transaction", new GetTransactionRequestHandler(transactionService));

        server.setExecutor(Executors.newFixedThreadPool(threads));

        return server;
    }
}
